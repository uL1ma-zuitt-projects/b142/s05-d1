package com.obrera.b142.s02.s02app.exceptions;

public class UserException extends Exception{

    public UserException(String message) {
        super(message);
    }
}
